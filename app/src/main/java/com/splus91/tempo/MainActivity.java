package com.splus91.tempo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity {

    TextView hours, minutes, modularHours;
    TextInputEditText number;
    Button updateTempoButton;
    double bpm;
    int minute;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        hours = (TextView) findViewById(R.id.hours);
        minutes = (TextView) findViewById(R.id.minutes);
        //number = (TextInputEditText) findViewById(R.id.number);
        modularHours = (TextView) findViewById(R.id.modular_hours);
        updateTempoButton = (Button) findViewById(R.id.update_tempo_button);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        updateTempoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                //       .setAction("Action", null).show();
                updateTempo();
            }
        });
    }

    private void updateTempo() {
        int modularHour = Integer.valueOf(modularHours.getText().toString());
        Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
        //long time = calendar.get(Calendar.SECOND) + (calendar.get(Calendar.MINUTE) * 60) + (calendar.get(Calendar.HOUR_OF_DAY) * 3600);
        //Double tempo = time + (time / 60 * 2.5);

        bpm = ((modularHour - 24) * 2.5) + 60;
        double time = (calendar.get(Calendar.MINUTE) + (calendar.get(Calendar.HOUR_OF_DAY) * 60)) * bpm / 60;
        int intTime = (int) time;
        int newHours = (int) time / 60;
        final String newMinutes = String.valueOf(intTime - newHours * 60);
        hours.setText(String.valueOf(newHours));
        minutes.setText(newMinutes);
        //if (Integer.valueOf(newMinutes) < 10 && Integer.valueOf(newMinutes) > 0){
        //    newMinutes += "0" + newMinutes;
        //}
        minute = Integer.valueOf(newMinutes);
//        ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
        double minuteModularSpeed = bpm;
        if (minuteModularSpeed == 0) {
            minuteModularSpeed = 1;
        }
        minute = minute - 1;
        int i = 1;
//        try {
//            while (true) {
//                minute = minute + 1;
//                minutes.setText(String.valueOf(minute));
//                Thread.sleep(Double.valueOf(minuteModularSpeed * 1000).longValue());
//            }
//        } catch (Exception e){
//
//        }
//        exec.scheduleAtFixedRate(new Runnable() {
//            @Override
//            public void run() {
//                minute = minute + 1;
//                minutes.setText(String.valueOf(minute));
//            }
//        }, 0, Double.valueOf(minuteModularSpeed * 1000).longValue(), TimeUnit.MILLISECONDS);
//        String text = newHours + ":" + newMinutes;
//        System.out.println(text);
//        Toast.makeText(getApplicationContext(), text, LENGTH_LONG).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void reduce(View view){
        if (Integer.valueOf(modularHours.getText().toString()) > 0){
            int number = Integer.valueOf(modularHours.getText().toString()) - 1;
            modularHours.setText(String.valueOf(number));
        } else {
            Toast.makeText(getBaseContext(), "You can't have less of 1 hour in a day", Toast.LENGTH_SHORT).show();
        }
    }

    public void increase(View view){
        if (Integer.valueOf(modularHours.getText().toString()) < 100){
            int number = Integer.valueOf(modularHours.getText().toString()) + 1;
            modularHours.setText(String.valueOf(number));
        }
    }

}
